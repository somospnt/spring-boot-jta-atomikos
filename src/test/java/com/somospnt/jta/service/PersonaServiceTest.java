package com.somospnt.jta.service;

import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PersonaServiceTest {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private DataSource dataSourcePersonas;

    @Autowired
    @Qualifier("dataSourceAlumnos")
    private DataSource dataSourceAlumnos;

    private JdbcTemplate jdbcTemplateOne;

    private JdbcTemplate jdbcTemplateTwo;

    @Before
    public void setup() {
        jdbcTemplateOne = new JdbcTemplate(dataSourcePersonas);
        jdbcTemplateTwo = new JdbcTemplate(dataSourceAlumnos);
    }

    @Test
    @Sql(scripts = {"/init-h2-personas.sql"}, config = @SqlConfig(dataSource = "dataSourcePersonas"))
    @Sql(scripts = {"/init-h2-alumnos.sql"}, config = @SqlConfig(dataSource = "dataSourceAlumnos"))
    public void transferirPorId_conDniNoExistente_Inserta() {
        int filasOneAntes = JdbcTestUtils.countRowsInTable(jdbcTemplateOne, "persona");
        int filasTwoAntes = JdbcTestUtils.countRowsInTable(jdbcTemplateTwo, "alumno");

        Assert.assertEquals(2, filasOneAntes);
        Assert.assertEquals(1, filasTwoAntes);

        personaService.transferirPorId(1L);

        int filasOneDespues = JdbcTestUtils.countRowsInTable(jdbcTemplateOne, "persona");
        int filasTwoDespues = JdbcTestUtils.countRowsInTable(jdbcTemplateTwo, "alumno");

        Assert.assertEquals(filasOneAntes - 1, filasOneDespues);
        Assert.assertEquals(filasTwoAntes + 1, filasTwoDespues);

    }

    @Test
    @Sql(scripts = {"/init-h2-personas.sql"}, config = @SqlConfig(dataSource = "dataSourcePersonas"))
    @Sql(scripts = {"/init-h2-alumnos.sql"}, config = @SqlConfig(dataSource = "dataSourceAlumnos"))
    public void transferirPorId_conDniExistente_haceRollback() {
        int filasOneAntes = JdbcTestUtils.countRowsInTable(jdbcTemplateOne, "persona");
        int filasTwoAntes = JdbcTestUtils.countRowsInTable(jdbcTemplateTwo, "alumno");

        Assert.assertEquals(2, filasOneAntes);
        Assert.assertEquals(1, filasTwoAntes);
        
        try {
            personaService.transferirPorId(2L);
        } catch (RuntimeException ex) {
            int filasOneDespues = JdbcTestUtils.countRowsInTable(jdbcTemplateOne, "persona");
            int filasTwoDespues = JdbcTestUtils.countRowsInTable(jdbcTemplateTwo, "alumno");

            Assert.assertEquals(filasOneAntes, filasOneDespues);
            Assert.assertEquals(filasTwoAntes, filasTwoDespues);
            return;
        }

        Assert.fail("Tendría que tirar error");

    }

}
