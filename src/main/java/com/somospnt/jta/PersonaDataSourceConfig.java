package com.somospnt.jta;

import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@DependsOn("transactionManager")
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryPersonas", basePackages = {"com.somospnt.jta.repository.persona"}, transactionManagerRef = "transactionManager")
public class PersonaDataSourceConfig {

    @Primary
    @Bean
    @ConfigurationProperties(prefix = "spring.jta.atomikos.datasource.personas")
    public DataSource dataSourcePersonas() {
        return new AtomikosDataSourceBean();
    }
    
    @Primary
    @Bean(name = "entityManagerFactoryPersonas")
    public EntityManagerFactory entityManagerFactory() {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
        properties.put("javax.persistence.transactionType", "JTA");
        
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        
        adapter.setShowSql(true);
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setJpaPropertyMap(properties);
        emf.setDataSource(dataSourcePersonas());
        emf.setPackagesToScan("com.somospnt.jta.domain.persona");
        emf.setJpaVendorAdapter(adapter);
        emf.setPersistenceUnitName("personas");
        emf.afterPropertiesSet();
        return emf.getObject();
    }

}
