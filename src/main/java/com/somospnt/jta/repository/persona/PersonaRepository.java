package com.somospnt.jta.repository.persona;

import com.somospnt.jta.domain.persona.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Long>{
    
}
