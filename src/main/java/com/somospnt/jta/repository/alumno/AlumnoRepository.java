package com.somospnt.jta.repository.alumno;

import com.somospnt.jta.domain.alumno.Alumno;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AlumnoRepository extends JpaRepository<Alumno, Long>{
    
}
