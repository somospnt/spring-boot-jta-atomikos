package com.somospnt.jta.service.impl;

import com.somospnt.jta.domain.persona.Persona;
import com.somospnt.jta.domain.alumno.Alumno;
import com.somospnt.jta.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.somospnt.jta.repository.persona.PersonaRepository;
import com.somospnt.jta.repository.alumno.AlumnoRepository;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

    @Autowired
    private PersonaRepository personaRepository;

    @Autowired
    private AlumnoRepository alumnoRepository;

    @Override
    public void transferirPorId(Long id) {
        Persona persona = personaRepository.findOne(id);
        if (persona != null) {
            Alumno alumno = new Alumno();
            alumno.setNombre(persona.getNombre());
            alumno.setDni(persona.getDni());
            personaRepository.delete(persona);
            alumnoRepository.save(alumno);
        }
    }

}
