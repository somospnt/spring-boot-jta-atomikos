package com.somospnt.jta;

import java.util.HashMap;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jta.atomikos.AtomikosDataSourceBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactoryAlumnos", basePackages = {"com.somospnt.jta.repository.alumno"}, transactionManagerRef = "transactionManager")
public class AlumnoDataSourceConfig {

    @Bean
    @ConfigurationProperties(prefix = "spring.jta.atomikos.datasource.alumnos")
    public DataSource dataSourceAlumnos() {
        return new AtomikosDataSourceBean();
    }

    @Bean(name = "entityManagerFactoryAlumnos")
    public EntityManagerFactory entityManagerFactory() {
        HashMap<String, Object> properties = new HashMap<>();
        properties.put("hibernate.transaction.jta.platform", AtomikosJtaPlatform.class.getName());
        properties.put("javax.persistence.transactionType", "JTA");
        
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();

        adapter.setShowSql(true);
        LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setJpaPropertyMap(properties);
        emf.setJpaVendorAdapter(adapter);
        emf.setDataSource(dataSourceAlumnos());
        emf.setPackagesToScan("com.somospnt.jta.domain.alumno");
        emf.setPersistenceUnitName("alumnos");
        emf.afterPropertiesSet();
        return emf.getObject();
    }

}
