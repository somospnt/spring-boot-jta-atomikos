DROP TABLE IF EXISTS alumno;

create table alumno (
  id BIGINT auto_increment NOT NULL PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  dni int NOT NULL UNIQUE
);

insert into alumno (id, nombre, dni)
values(1, 'tomi', 24036789);