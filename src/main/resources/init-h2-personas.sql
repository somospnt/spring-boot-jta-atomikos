DROP TABLE IF EXISTS persona;

create table persona (
  id BIGINT auto_increment NOT NULL PRIMARY KEY,
  nombre varchar(255) NOT NULL,
  dni int NOT NULL UNIQUE
);

insert into persona (id, nombre, dni)
values(1, 'tomi', 24036790),
(2, 'jose', 24036789);
